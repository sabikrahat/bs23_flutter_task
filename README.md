# Brain Station 23 Flutter Task

Developed a simple Flutter application that shows the most starred GitHub repositories by searching with the key word "Flutter". The application has two screens. The first screen shows the list of repositories and the second screen shows the details of the selected repository. The application uses the GitHub API to fetch the data.

## UI Design

<table>
  <tr>
    <td><img src="ui/sample-1.png" width=250 height=480></td>
    <td><img src="ui/sample-2.png" width=250 height=480></td>
    <td><img src="ui/sample-5.png" width=250 height=480></td>
  </tr>
  <tr>
    <td><img src="ui/sample-9.png" width=250 height=480></td>
    <td><img src="ui/sample-3.png" width=250 height=480></td>
    <td><img src="ui/sample-4.png" width=250 height=480></td>
  </tr>
  <tr>
    <td><img src="ui/sample-6.png" width=250 height=480></td>
    <td><img src="ui/sample-7.png" width=250 height=480></td>
    <td><img src="ui/sample-8.png" width=250 height=480></td>
  </tr>
 </table>

## Features
- Show the most starred GitHub repositories by searching with the key word "Flutter".
- Show the list of repositories.
- Show the details of the selected repository.
- Save the repositories in the local database using the Hive package.
- Realtime switching data between the local database and the API base on the network status.
- Show the network status.
- Show the loading indicator while fetching data from the API.
- Api error handling.
- Show the error message if the API fails to fetch data.
- Refresh the data by pulling down the list. Interval time is 30 minutes.
- Dark and light theme support.
- Change the language (English and Bengali).
- Change the Date and Time format.
- Change font style.
- View the local database data.
- Clear the local database data.
- Show the app performance graph.