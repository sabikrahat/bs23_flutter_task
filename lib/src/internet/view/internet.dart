import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../config/constants.dart';
import '../../utils/extensions/extensions.dart';
import '../provider/internet.dart';

class InternetWidget extends ConsumerWidget {
  const InternetWidget({super.key, this.message});

  final String? message;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final isConnected = ref.watch(isConnectedPd);
    return isConnected
        ? const SizedBox()
        : _NoInternet(message ?? 'No internet connection');
  }
}

class _NoInternet extends StatelessWidget {
  const _NoInternet(this.message);

  final String message;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 25.0,
      color: Colors.red[100],
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: mainCenter,
        children: [
          Icon(
            Icons.wifi_off,
            color: Colors.red[900],
            size: 15.0,
          ),
          const SizedBox(width: 5.0),
          Text(
            message,
            style: context.text.labelMedium!.copyWith(
              color: Colors.red[900],
            ),
          ),
        ],
      ),
    );
  }
}
