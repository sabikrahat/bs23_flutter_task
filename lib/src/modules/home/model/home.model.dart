import 'dart:convert';

import 'package:hive/hive.dart';

import '../../../db/hive.dart';

part 'home.model.crud.ext.dart';
part 'home.model.g.dart';

@HiveType(typeId: HiveTypes.gitRepo)
class GitRepo extends HiveObject {
  @HiveField(0)
  int id;
  @HiveField(1)
  String name;
  @HiveField(2)
  String? description;
  @HiveField(3)
  Owner owner;
  @HiveField(4)
  DateTime createdAt;
  @HiveField(5)
  DateTime? updatedAt;
  @HiveField(6)
  DateTime? pushedAt;
  @HiveField(7)
  int stargazersCount;
  @HiveField(8)
  List<String> topics;

  GitRepo({
    required this.id,
    required this.name,
    this.description,
    required this.owner,
    required this.createdAt,
    required this.updatedAt,
    required this.pushedAt,
    required this.stargazersCount,
    required this.topics,
  });

  factory GitRepo.fromJson(Map<String, dynamic> json) => GitRepo(
        id: json['id'] as int,
        name: json['name'] as String,
        description: json['description'],
        owner: Owner.fromJson(json['owner'] as Map<String, dynamic>),
        createdAt: DateTime.parse(json['created_at'] as String),
        updatedAt: json['updated_at'] == null ? null : DateTime.parse(json['updated_at']),
        pushedAt: json['pushed_at'] == null ? null : DateTime.parse(json['pushed_at']),
        stargazersCount: json['stargazers_count'] as int,
        topics: List<String>.from(json['topics'] as List<dynamic>),
      );

  factory GitRepo.fromRawJson(String source) =>
      GitRepo.fromJson(json.decode(source));

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'owner': owner.toJson(),
        'created_at': createdAt.toIso8601String(),
        'updated_at': updatedAt?.toIso8601String(),
        'pushed_at': pushedAt?.toIso8601String(),
        'stargazers_count': stargazersCount,
        'topics': topics,
      };

  String toRawJson() => json.encode(toJson());

  @override
  String toString() {
    return 'GitRepo{id: $id, name: $name, description: $description, owner: $owner, createdAt: $createdAt, updatedAt: $updatedAt, pushedAt: $pushedAt, stargazersCount: $stargazersCount, topics: $topics}';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is GitRepo && other.id == id;
  }

  @override
  int get hashCode => id.hashCode;
}

@HiveType(typeId: HiveTypes.owner)
class Owner extends HiveObject {
  @HiveField(0)
  int id;
  @HiveField(1)
  String login;
  @HiveField(2)
  String avatarUrl;

  Owner({
    required this.id,
    required this.login,
    required this.avatarUrl,
  });

  factory Owner.fromJson(Map<String, dynamic> json) => Owner(
        id: json['id'] as int,
        login: json['login'] as String,
        avatarUrl: json['avatar_url'] as String,
      );

  factory Owner.fromRawJson(String source) =>
      Owner.fromJson(json.decode(source));

  Map<String, dynamic> toJson() => {
        'id': id,
        'login': login,
        'avatar_url': avatarUrl,
      };

  String toRawJson() => json.encode(toJson());

  @override
  String toString() => 'Owner{id: $id, login: $login, avatarUrl: $avatarUrl}';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Owner && other.id == id && other.login == login;
  }

  @override
  int get hashCode => id.hashCode ^ login.hashCode;
}
