part of 'home.model.dart';

extension GitRepoDExt on GitRepo {

  Future<void> saveData() async => await Boxes.gitRepos.put(id, this);

  Future<void> deleteData() async => await Boxes.gitRepos.delete(id);
}

extension ListGitRepoDExt on List<GitRepo> {
  Future<void> saveAllData() async => await Boxes.gitRepos
      .putAll(Map.fromEntries(map((e) => MapEntry(e.id, e))));

  Future<void> deleteAllData() async =>
      await Boxes.gitRepos.deleteAll(map((e) => e.id).toList());
}
