import 'dart:convert';
import 'dart:io';

import '../model/home.model.dart';
import 'package:http/http.dart' as http;

import '../../../utils/logger/logger_helper.dart';

Future<List<GitRepo>> getRepos({
  String q = 'flutter',
  String sort = 'stars',
  String order = 'desc',
  int perPage = 10,
  int page = 1,
}) async {
  try {
    var request = http.Request(
        'GET',
        Uri.parse(
            'https://api.github.com/search/repositories?q=$q&sort=$sort&order=$order&per_page=$perPage&page=$page'));

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      final body = await response.stream.bytesToString();
      final data = json.decode(body);
      log.f('Response: $data');
      List<GitRepo> gitRepos = [];
      for (var item in data['items']) {
        gitRepos.add(GitRepo.fromJson(item));
      }
      return gitRepos;
    } else {
      log.e('Failed to load data');
      throw Exception('${response.statusCode}\nFailed to load data.');
    }
  } on SocketException {
    throw Exception('No internet connection');
  } on HttpException {
    throw Exception('Failed to connect to server');
  } on FormatException {
    throw Exception('Error decoding response');
  } catch (e) {
    log.e('Failed to load data. $e');
    throw Exception('Failed to load data. $e');
  }
}
