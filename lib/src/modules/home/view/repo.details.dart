import '../../../db/db.dart';
import '../../settings/model/settings.model.dart';
import '../../../shared/error_widget/error_widget.dart';
import '../../../utils/extensions/extensions.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../config/constants.dart';
import '../provider/home.provider.dart';

class RepoDetailsView extends ConsumerWidget {
  const RepoDetailsView(this.id, {super.key});

  final String? id;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    if (id == null) const KErrorWidget(error: 'No id found!');
    final repo = ref.read(homeProvider.notifier).getRepo(int.parse(id!));
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => context.beamBack(),
        ),
        title: const Text('Details'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Column(
          crossAxisAlignment: crossStart,
          children: [
            const InfoTitle('Owner Info'),
            ListTile(
              leading: Container(
                width: 50.0,
                height: 50.0,
                padding: const EdgeInsets.all(4.0),
                decoration: BoxDecoration(
                  borderRadius: borderRadius45,
                  color: context.theme.primaryColor.withOpacity(0.2),
                  border: Border.all(
                    color: context.theme.primaryColor,
                    width: 1.5,
                  ),
                ),
                child: ClipRRect(
                  borderRadius: borderRadius45,
                  child: CachedNetworkImage(
                    imageUrl: repo.owner.avatarUrl,
                    progressIndicatorBuilder: (_, __, p) => Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CircularProgressIndicator(
                        strokeWidth: 2.5,
                        value: p.progress,
                      ),
                    ),
                    errorWidget: (context, url, error) =>
                        const Center(child: Icon(Icons.error)),
                  ),
                ),
              ),
              title: Text(repo.owner.login),
              subtitle: Text(
                  'Last updated: ${repo.updatedAt == null ? '...' : appSettings.getDateTimeFormat.format(repo.updatedAt!)}'),
            ),
            const InfoTitle('Repo Info'),
            Row(
              mainAxisAlignment: mainSpaceBetween,
              children: [
                Text(repo.name, style: context.text.titleLarge),
                Row(
                  children: [
                    const Icon(
                      Icons.star,
                      size: 16.0,
                      color: Colors.amber,
                    ),
                    const SizedBox(width: 2.0),
                    Text(
                      repo.stargazersCount.toString(),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 10.0),
            Text(repo.description ?? 'No description available',
                textAlign: TextAlign.justify, style: context.text.bodyMedium),
            const SizedBox(height: 10.0),
            Wrap(
              children: List.generate(
                repo.topics.length,
                (index) => Padding(
                  padding: const EdgeInsets.only(right: 5.0),
                  child: Chip(
                    label: Text(repo.topics[index]),
                    backgroundColor: context.theme.primaryColor,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10.0),
            Text(
              'Repo Created: ${appSettings.getDateTimeFormat.format(repo.createdAt)}',
              style: context.text.labelLarge,
            ),
            const SizedBox(height: 5.0),
            Text(
              'Last Updated: ${repo.updatedAt == null ? '...' : appSettings.getDateTimeFormat.format(repo.updatedAt!)}',
              style: context.text.labelLarge,
            ),
            const SizedBox(height: 5.0),
            Text(
              'Last Pushed: ${repo.pushedAt == null ? '...' : appSettings.getDateTimeFormat.format(repo.pushedAt!)}',
              style: context.text.labelLarge,
            ),
          ],
        ),
      ),
    );
  }
}

class InfoTitle extends StatelessWidget {
  const InfoTitle(this.label, {super.key});

  final String label;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 10.0),
      child: Row(
        mainAxisAlignment: mainCenter,
        children: [
          Text(
            '• $label',
            style: context.text.labelLarge!
                .copyWith(color: context.theme.primaryColor),
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 10.0, top: 2.0),
              height: 1.0,
              color: context.theme.primaryColor,
            ),
          )
        ],
      ),
    );
  }
}
