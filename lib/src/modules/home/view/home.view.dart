import 'package:bs23_flutter_task/src/utils/themes/themes.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../../app.routes.dart';
import '../../../config/constants.dart';
import '../../../db/db.dart';
import '../../../localization/loalization.dart';
import '../../../shared/error_widget/error_widget.dart';
import '../../../shared/loading_widget/loading_widget.dart';
import '../../../shared/page_not_found/page_not_found.dart';
import '../../../utils/extensions/extensions.dart';
import '../../settings/model/settings.model.dart';
import '../provider/home.provider.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(t.home),
        actions: [
          IconButton(
              icon: const Icon(Icons.settings),
              onPressed: () => context.beamPush(AppRoutes.settingsRoute)),
        ],
      ),
      body: const HomeBody(),
    );
  }
}

class HomeBody extends ConsumerWidget {
  const HomeBody({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final notifier = ref.watch(homeProvider.notifier);
    return ref.watch(homeProvider).when(
          loading: () => const LoadingWidget(),
          error: (error, stack) => KErrorWidget(error: error),
          data: (_) => SmartRefresher(
            controller: ref.watch(homeProvider.notifier).refreshController,
            enablePullUp: true,
            onRefresh: ref.watch(homeProvider.notifier).refresh,
            onLoading: ref.watch(homeProvider.notifier).loadMore,
            child: CustomScrollView(
              slivers: [
                SliverPersistentHeader(
                  pinned: true,
                  delegate: _StickyTabBarDelegate(notifier),
                ),
                notifier.gitRepos.isEmpty
                    ? const SliverFillRemaining(
                        child: KDataNotFound(
                          msg: 'No Data Found!',
                        ),
                      )
                    : SliverFixedExtentList(
                        itemExtent: 85.0,
                        delegate: SliverChildBuilderDelegate(
                          childCount: notifier.gitRepos.length,
                          (_, index) {
                            final repo = notifier.gitRepos[index];
                            return InkWell(
                              borderRadius: borderRadius5,
                              onTap: () => context.beamPush(
                                  '${AppRoutes.detailsRoute}/${repo.id}'),
                              child: Card(
                                elevation: 1.0,
                                margin: const EdgeInsets.fromLTRB(
                                    6.0, 6.0, 6.0, 0.0),
                                shape: roundedRectangleBorder5,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 7.0, vertical: 5.0),
                                  child: Row(
                                    crossAxisAlignment: crossStart,
                                    mainAxisAlignment: mainCenter,
                                    children: [
                                      Expanded(
                                        flex: 3,
                                        child: Column(
                                          mainAxisSize: mainMin,
                                          crossAxisAlignment: crossStart,
                                          children: [
                                            Text(
                                              repo.name,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: context.text.titleMedium,
                                            ),
                                            const SizedBox(height: 3.0),
                                            Text(
                                              repo.description ??
                                                  'No Description Found!',
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: context.text.bodyMedium,
                                            ),
                                            const SizedBox(height: 3.0),
                                            Row(
                                              children: [
                                                const Icon(
                                                  Icons.star,
                                                  size: 16.0,
                                                  color: Colors.amber,
                                                ),
                                                const SizedBox(width: 2.0),
                                                Text(
                                                  repo.stargazersCount
                                                      .toString(),
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style:
                                                      context.text.labelMedium,
                                                ),
                                                const SizedBox(width: 8.0),
                                                Icon(
                                                  Icons.update,
                                                  size: 16.0,
                                                  color: Colors.grey[700],
                                                ),
                                                const SizedBox(width: 2.0),
                                                Text(
                                                  repo.updatedAt == null
                                                      ? '...'
                                                      : appSettings
                                                          .getDateTimeFormat
                                                          .format(
                                                              repo.updatedAt!),
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style:
                                                      context.text.labelMedium,
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(width: 3.0),
                                      Expanded(
                                        flex: 1,
                                        child: Column(
                                          mainAxisSize: mainMin,
                                          mainAxisAlignment: mainCenter,
                                          crossAxisAlignment: crossEnd,
                                          children: [
                                            ClipRRect(
                                              borderRadius: borderRadius45,
                                              child: SizedBox(
                                                width: 40.0,
                                                height: 40.0,
                                                child: CachedNetworkImage(
                                                  imageUrl:
                                                      repo.owner.avatarUrl,
                                                  progressIndicatorBuilder:
                                                      (_, __, p) => Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            8.0),
                                                    child:
                                                        CircularProgressIndicator(
                                                      strokeWidth: 2.5,
                                                      value: p.progress,
                                                    ),
                                                  ),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          const Center(
                                                              child: Icon(
                                                                  Icons.error)),
                                                ),
                                              ),
                                            ),
                                            const SizedBox(height: 3.0),
                                            Text(
                                              repo.owner.login,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: context.text.labelMedium,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
              ],
            ),
          ),
        );
  }
}

class _StickyTabBarDelegate extends SliverPersistentHeaderDelegate {
  final HomeProvider notifier;

  _StickyTabBarDelegate(this.notifier);
  @override
  Widget build(BuildContext context, _, __) {
    return InkWell(
      borderRadius: borderRadius5,
      onTap: () async => await notifier.toggleSortBy(),
      child: Container(
        margin: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          color: notifier.isConnected ? Colors.green[100] : Colors.red[100],
          borderRadius: borderRadius5,
          border: Border.all(
            color: notifier.isConnected ? Colors.green : Colors.red,
            width: 1.0,
          ),
        ),
        child: Center(
          child: Column(
            mainAxisSize: mainMin,
            children: [
              Row(
                mainAxisAlignment: mainCenter,
                children: [
                  Icon(
                    notifier.isConnected ? Icons.wifi : Icons.signal_wifi_off,
                    color: notifier.isConnected ? Colors.green : Colors.red,
                    size: 20.0,
                  ),
                  const SizedBox(width: 5.0),
                  Text(
                    notifier.isConnected
                        ? 'Showing live data & sorting by ${notifier.sortByStars ? 'stars' : 'last updated time'}.'
                        : 'Using Local db & sorting by ${notifier.sortByStars ? 'stars' : 'last updated time'}.',
                    style: context.text.labelLarge!.copyWith(color: black),
                  ),
                ],
              ),
              const SizedBox(height: 2.0),
              Row(
                mainAxisAlignment: mainCenter,
                children: [
                  const Icon(
                    Icons.sync,
                    color: black,
                    size: 20.0,
                  ),
                  const SizedBox(width: 5.0),
                  Text(
                    'Tap to sort by ${notifier.sortByStars ? 'last updated time' : 'stars'}',
                    style: context.text.labelLarge!.copyWith(color: black),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  double get maxExtent => 60.0; // Height of the tab bar when expanded

  @override
  double get minExtent => 60.0; // Height of the tab bar when collapsed

  @override
  bool shouldRebuild(covariant _StickyTabBarDelegate oldDelegate) => true;
}
