import 'dart:async';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../db/db.dart';
import '../../../db/hive.dart';
import '../../../internet/provider/internet.dart';
import '../api/home.api.dart';
import '../model/home.model.dart';

typedef HomeNotifier = AsyncNotifierProvider<HomeProvider, List<GitRepo>?>;

final homeProvider = HomeNotifier(HomeProvider.new);

class HomeProvider extends AsyncNotifier<List<GitRepo>?> {
  DateTime lastUpdated = DateTime.now();
  int page = 1;
  bool isLoading = false;
  final refreshController = RefreshController(initialRefresh: false);
  late bool isConnected;
  late bool _sortByStars;

  List<GitRepo> gitRepos = [];

  @override
  FutureOr<List<GitRepo>?> build() async {
    isConnected = ref.watch(isConnectedPd);
    _sortByStars = appSettings.isSortByStars;
    await _initial();
    return gitRepos;
  }

  _initial([bool showLoader = false]) async {
    if (showLoader) EasyLoading.show();
    if (isConnected) {
      gitRepos =
          await getRepos(page: page, sort: _sortByStars ? 'stars' : 'updated');
      // await Boxes.gitRepos.clear();
      await gitRepos.saveAllData();
    } else {
      final temp = Boxes.gitRepos.values.toList();
      if (temp.isNotEmpty) {
        if (_sortByStars) {
          temp.sort((a, b) => b.stargazersCount.compareTo(a.stargazersCount));
        } else {
          temp.sort((a, b) => (b.updatedAt ?? b.createdAt)
              .compareTo(a.updatedAt ?? a.createdAt));
        }
        gitRepos = temp;
      }
    }
    if (showLoader) EasyLoading.dismiss();
  }

  GitRepo getRepo(int id) => gitRepos.firstWhere((repo) => repo.id == id);

  bool get sortByStars => _sortByStars;

  Future<void> toggleSortBy() async {
    _sortByStars = !_sortByStars;
    await _initial(true);
    appSettings.isSortByStars = _sortByStars;
    await appSettings.save();
    ref.notifyListeners();
  }

  Future<void> loadMore() async {
    if (!isConnected) {
      refreshController.loadFailed();
      EasyLoading.showToast(
        'No internet connection!',
        duration: const Duration(seconds: 1),
        toastPosition: EasyLoadingToastPosition.bottom,
      );
      return;
    }
    if (isLoading) return;
    isLoading = true;
    page++;
    final repos =
        await getRepos(page: page, sort: _sortByStars ? 'stars' : 'updated');
    if (repos.isNotEmpty) {
      gitRepos.addAll(repos);
      await gitRepos.saveAllData();
    }
    refreshController.loadComplete();
    isLoading = false;
    ref.notifyListeners();
  }

  bool get canUpdate {
    final now = DateTime.now();
    final diff = now.difference(lastUpdated);
    return diff.inMinutes > 30; // as per requirement
  }

  Future<void> refresh() async {
    if (!canUpdate) {
      refreshController.refreshFailed();
      EasyLoading.showToast(
        'You can\'t update too soon!',
        duration: const Duration(seconds: 1),
        toastPosition: EasyLoadingToastPosition.bottom,
      );
      return;
    }
    if (!isConnected) {
      refreshController.refreshFailed();
      EasyLoading.showToast(
        'No internet connection!',
        duration: const Duration(seconds: 1),
        toastPosition: EasyLoadingToastPosition.bottom,
      );
      return;
    }
    page = 1;
    final repos =
        await getRepos(page: page, sort: _sortByStars ? 'stars' : 'updated');
    if (repos.isNotEmpty) {
      gitRepos = repos;
      await Boxes.gitRepos.clear();
      await gitRepos.saveAllData();
    }
    lastUpdated = DateTime.now();
    refreshController.refreshCompleted();
    ref.notifyListeners();
  }
}
