import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import '../../../../db/hive.dart';
import '../../../../localization/loalization.dart';

part 'locale.model.ext.dart';
part 'locale.model.g.dart';


@HiveType(typeId: HiveTypes.localeProfile)
enum LocaleProfile {
  @HiveField(0)
  bengali,
  @HiveField(1)
  english,
}
