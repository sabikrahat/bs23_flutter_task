part of 'locale.model.dart';

extension LocaleProfileExtension on LocaleProfile {
  Locale get locale {
    switch (this) {
      case LocaleProfile.bengali:
        return bnLocale;
      case LocaleProfile.english:
        return enLocale;
      default:
        return enLocale;
    }
  }

  String get localeName {
    switch (this) {
      case LocaleProfile.bengali:
        return 'Bengali';
      case LocaleProfile.english:
        return 'English';
      default:
        return 'English';
    }
  }
}
