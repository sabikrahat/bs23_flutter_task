import 'package:hive/hive.dart';

import '../modules/home/model/home.model.dart';
import '../modules/settings/model/locale/locale.model.dart';
import '../modules/settings/model/settings.model.dart';
import '../modules/settings/model/theme/theme.model.dart';
import 'hive.dart';

class HiveFuntions {
  static void registerHiveAdepters() {
    Hive.registerAdapter(LocaleProfileAdapter());
    Hive.registerAdapter(ThemeProfileAdapter());
    Hive.registerAdapter(AppSettingsAdapter());
    Hive.registerAdapter(GitRepoAdapter());
    Hive.registerAdapter(OwnerAdapter());
  }

  static Future<void> openAllBoxes() async {
    await Hive.openBox<LocaleProfile>(BoxNames.localeProfile);
    await Hive.openBox<ThemeProfile>(BoxNames.themeProfile);
    await Hive.openBox<AppSettings>(BoxNames.appSettings);
    await Hive.openBox<GitRepo>(BoxNames.gitRepo);
    await Hive.openBox<Owner>(BoxNames.owner);
  }

  static Future<void> closeAllBoxes() async {
    await Boxes.localeProfile.close();
    await Boxes.themeProfile.close();
    await Boxes.appSettings.close();
    await Boxes.gitRepos.close();
    await Boxes.owners.close();
  }

  static Future<void> clearAllBoxes() async {
    await Boxes.localeProfile.clear();
    await Boxes.themeProfile.clear();
    await Boxes.appSettings.clear();
    await Boxes.gitRepos.clear();
    await Boxes.owners.clear();
  }

  static Future<void> deleteAllBoxes() async {
    await Hive.deleteBoxFromDisk(BoxNames.localeProfile);
    await Hive.deleteBoxFromDisk(BoxNames.themeProfile);
    await Hive.deleteBoxFromDisk(BoxNames.appSettings);
    await Hive.deleteBoxFromDisk(BoxNames.gitRepo);
    await Hive.deleteBoxFromDisk(BoxNames.owner);
  }
}
