import 'package:beamer/beamer.dart'
    show
        BeamGuard,
        BeamPage,
        BeamPageType,
        BeamerDelegate,
        RoutesLocationBuilder;
import 'package:bs23_flutter_task/src/modules/settings/view/setting.view.dart';
import 'package:flutter/widgets.dart' show ValueKey;

import 'app.routes.dart';
import 'src/config/constants.dart';
import 'src/modules/home/view/home.view.dart';
import 'src/modules/home/view/repo.details.dart';
import 'src/shared/page_not_found/page_not_found.dart';

final routerDelegate = BeamerDelegate(
  initialPath: AppRoutes.homeRoute,
  notFoundPage: const BeamPage(
    key: ValueKey('not-found'),
    title: 'Page not found - $appName',
    child: KPageNotFound(error: '404 - Page not found!'),
  ),
  locationBuilder: RoutesLocationBuilder(
    routes: {
      AppRoutes.homeRoute: (_, __, ___) {
        return const BeamPage(
          key: ValueKey(AppRoutes.homeRoute),
          title: appName,
          type: BeamPageType.fadeTransition,
          child: HomeView(),
        );
      },
      AppRoutes.settingsRoute: (_, __, ___) {
        return const BeamPage(
          key: ValueKey(AppRoutes.settingsRoute),
          title: appName,
          type: BeamPageType.fadeTransition,
          child: SettingsView(),
        );
      },
      '${AppRoutes.detailsRoute}/:id': (_, state, __) {
        final id = state.pathParameters['id'];
        return BeamPage(
          key: ValueKey(id),
          title: 'Details - $appName',
          type: BeamPageType.fadeTransition,
          child: RepoDetailsView(id),
        );
      },
    },
  ).call,
  guards: [
    BeamGuard(
      pathPatterns: AppRoutes.allRoutes,
      check: (_, __) => !AppRoutes.isMaintenanceBreak,
      beamToNamed: (_, __, ___) => AppRoutes.maintenanceBreakRoute,
    ),
    BeamGuard(
      pathPatterns: [AppRoutes.maintenanceBreakRoute],
      check: (_, __) => AppRoutes.isMaintenanceBreak,
      beamToNamed: (_, __, ___) => AppRoutes.homeRoute,
    ),
  ],
);
