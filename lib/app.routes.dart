class AppRoutes {
  ///
  /// isMaintenanceBreak is a global variable that is set to true when the app
  static const isMaintenanceBreak = false;

  ///
  static const String homeRoute = '/home';
  static const String detailsRoute = '/details';
  static const String maintenanceBreakRoute = '/maintenance-break';
  static const String serverDisconnectedRoute = '/server-disconnected';
  static const String settingsRoute = '/settings';

  static const List<String> allRoutes = [
    homeRoute,
    detailsRoute,
    maintenanceBreakRoute,
    settingsRoute,
  ];

}
